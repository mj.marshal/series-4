package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Node implements NodeInterface {

    private final String document;
    private String tag;
    private boolean isSingle;
    private String inside;
    private HashMap<String, String> attributes;
    private List<Node> children;

    public Node(String document) {
        this.document = document;
        extract();
    }

    private void extract(){
        findTag();
        if( document.charAt(1) == '/' )
            this.isSingle = true;
        findInside();
        findAttributes();
        findChildren();
    }

    private void findTag(){
        StringBuilder tagName = new StringBuilder();
        int i = document.indexOf('<') + 1;
        while( document.charAt(i) != ' ' && document.charAt(i) != '>'){
            tagName.append(document.charAt(i));
            i++;
        }
        String res = tagName.toString();
        if( res.contains("/") )
            res = res.replace("/","");
        this.tag = res;
    }

    private void findInside(){
        if( isSingle ) {
            this.inside = "";
            return;
        }
        try {
            this.inside = document.substring(document.indexOf('>') + 1, document.length() - tag.length() - 3);
        }
        catch (Exception e){
            this.inside = "";
        }
    }

    private void findAttributes(){
        int start = 0;
        int end = 0;
        boolean check = false;
        for(int i = 0; i < document.length(); i++ ){
            if( document.charAt(i) == '>' ){
                if( check ){
                    end = i;
                    break;
                }
                this.attributes = null;
                return;
            }
            else if( document.charAt(i) == ' ' && !check ){
                start = i + 1;
                check = true;
            }
        }
        attributes = new HashMap<>();
        String attrs = document.substring(start,end);
        int i = 0;
        StringBuilder key = new StringBuilder();
        StringBuilder value = new StringBuilder();
        while( i < attrs.length() ){
            if( attrs.charAt(i) == ' ' )
                i ++;
            else{
                while( attrs.charAt(i) != '=' ){
                    key.append(attrs.charAt(i));
                    i++;
                }
                i += 2;
                while( attrs.charAt(i) != '\"' ){
                    value.append(attrs.charAt(i));
                    i++;
                }
                i++;
                attributes.put(key.toString(),value.toString());
                System.out.println(key + "   " + value);
                key.delete(0,key.length());
                value.delete(0,value.length());
            }
        }
    }

    private void findChildren(){
        children = new ArrayList<>();
        if( inside.equals("") )
            return;
        int i = 0;
        while( i < inside.length() ){
            if( inside.charAt(i) == '<' ){
                if( inside.charAt(i+1) == '/' ){
                    StringBuilder child = new StringBuilder();
                    while( inside.charAt(i) != '>' ){
                        child.append(inside.charAt(i));
                        i++;
                    }
                    child.append(inside.charAt(i));
                    i++;
                    children.add(new Node(child.toString()));
                }
                else {
                    StringBuilder tName = new StringBuilder();
                    StringBuilder child = new StringBuilder();
                    child.append(inside.charAt(i));
                    i++;
                    while (inside.charAt(i) != '>' && inside.charAt(i) != ' ' ){
                        tName.append(inside.charAt(i));
                        child.append(inside.charAt(i));
                        i++;
                    }
                    boolean check = false;
                    while ( !check ){
                        if( inside.charAt(i) == '/' ){
                            StringBuilder temp = new StringBuilder();
                            while (inside.charAt(i) != '>' && inside.charAt(i) != ' ' ){
                                child.append(inside.charAt(i));
                                temp.append(inside.charAt(i));
                                i++;
                            }
                            child.append(inside.charAt(i));
                            i++;
                            if( temp.toString().equals( "/" + tName) ){
                                children.add(new Node(child.toString()));
                                check = true;
                            }
                        }
                        else {
                            child.append(inside.charAt(i));
                            i++;
                        }
                    }
                }
            }
            else if( inside.charAt(i) == ' '){
                i++;
            }
            else {
                return;
            }
        }
    }

    @Override
    public String getStringInside() {
        if( inside.equals(""))
            return null;
        return inside;
    }

    @Override
    public List<Node> getChildren() {
        return children;
    }

    @Override
    public String getAttributeValue(String key) {
        // TODO implement this
        return attributes.get(key);
    }
}
