package sbu.cs.parser.json;

import java.util.ArrayList;

public class Json implements JsonInterface {

    ArrayList<JsonObj> jElements;

    public Json( ArrayList<JsonObj> jElements ){
        this.jElements = jElements;
    }

    @Override
    public String getStringValue(String key) {
        for (JsonObj jElement : jElements)
            if (jElement.getKey().equals(key))
                return jElement.getValue();
        throw new IllegalArgumentException();
    }
}
