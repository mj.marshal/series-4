package sbu.cs.parser.json;

public class JsonDouble implements JsonObj {
    private double value;
    private String key;

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getValue() {
        return value + "";
    }
    public void setValue(double value) {
        this.value = value;
    }
}
