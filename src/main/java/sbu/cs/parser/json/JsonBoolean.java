package sbu.cs.parser.json;

public class JsonBoolean implements JsonObj {
    private boolean value;
    private String key;

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getValue() {
        return value + "";
    }
    public void setValue(boolean value) {
        this.value = value;
    }
}
