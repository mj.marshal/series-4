package sbu.cs.parser.json;

public class JsonInteger implements JsonObj {
    private int value;
    private String key;

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getValue() {
        return value + "";
    }
    public void setValue(int value) {
        this.value = value;
    }
}
