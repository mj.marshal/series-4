package sbu.cs.parser.json;

import java.util.ArrayList;

public class JsonParser {

    public static Json parse(String data) {

        ArrayList<JsonObj> jElements = new ArrayList<>();

        int i = 0;
        StringBuilder key = new StringBuilder();
        StringBuilder value = new StringBuilder();
        while (i < data.length() ){
            if( data.charAt(i) == '\"' ){
                i++;
                while (data.charAt(i) != '\"' ){
                    key.append(data.charAt(i));
                    i++;
                }
                i++;
            }
            else if( data.charAt(i) == ':' ){
                boolean started = false;
                boolean isArray = false;
                i++;
                while ( (data.charAt(i) != ',' && data.charAt(i) != '}') || isArray ){
                    if(data.charAt(i) == '[' )
                        isArray = true;
                    if(data.charAt(i) == ']' )
                        isArray = false;
                    if( data.charAt(i) == ' ' && !started ){
                        i++;
                    }
                    else {
                        value.append(data.charAt(i));
                        i++;
                        started = true;
                    }
                }
                i++;
                String strVal = value.toString().replaceAll("\"" , "");
                strVal = strVal.replaceAll("\n","");
                String strKey = key.toString();
                value.delete(0,value.length());
                key.delete(0,key.length());
                if( strVal.matches("\\d+")){
                    JsonInteger jsonInteger = new JsonInteger();
                    jsonInteger.setKey(strKey);
                    jsonInteger.setValue(Integer.parseInt(strVal));
                    jElements.add(jsonInteger);
                }
                else if( strVal.matches("/[0-9]+(\\.[0-9]+)?$") ){
                    JsonDouble jsonDouble = new JsonDouble();
                    jsonDouble.setKey(strKey);
                    jsonDouble.setValue(Double.parseDouble(strVal));
                    jElements.add(jsonDouble);
                }
                else if(strVal.matches("^(true|false)$")){
                    JsonBoolean jsonBoolean = new JsonBoolean();
                    jsonBoolean.setKey(strKey);
                    jsonBoolean.setValue(Boolean.parseBoolean(strVal));
                    jElements.add(jsonBoolean);
                }
                else {
                    JsonString jsonString = new JsonString();
                    jsonString.setKey(strKey);
                    jsonString.setValue(strVal);
                    jElements.add(jsonString);
                }
            }
            else {
                i++;
            }
        }
        return new Json(jElements);
    }
}
